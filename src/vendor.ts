// RxJS
import 'rxjs';
// Other vendors for example jQuery, Lodash or Bootstrap
// You can import js, ts, css, sass, ...
import 'SpinKit/scss/spinners/3-wave.scss';
import 'Ionicons/scss/ionicons.scss';
import 'font-awesome/less/font-awesome.less';
import 'weather-icons/less/weather-icons.less';
import 'bootstrap/less/bootstrap.less';
import 'daterangepicker/daterangepicker.scss';
import 'jquery-ui/themes/base/datepicker.css';
import 'jquery-toggles/css/toggles-full.css';
import 'datatables.net-bs/css/dataTables.bootstrap.css';
import 'datatables.net-responsive-bs/css/responsive.bootstrap.css';
import 'datatables.net-select-bs/css/select.bootstrap.css';
import 'sweetalert2/src/sweetalert2.scss';
import 'select2/dist/css/select2.min.css';
import 'froala-editor/css/froala_editor.pkgd.min.css';
import 'froala-editor/css/froala_style.min.css';
import './assets/css/quirk.css';
import './assets/css/dataTables.colVis.css';

import 'bootstrap';
import 'daterangepicker/daterangepicker';
import 'jquery-ui/ui/widgets/datepicker';
import 'jquery-toggles';
import 'datatables.net';
import 'datatables.net-bs';
import 'datatables.net-colreorder';
import 'datatables.net-responsive';
import 'datatables.net-responsive-bs';
import 'datatables.net-select';
import 'sweetalert2';
import 'select2';
import 'froala-editor';
import './assets/js/dataTables.colVis.js';