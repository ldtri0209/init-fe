import {NgModule} from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';

import {AuthGuard} from './_guards/auth.guard';

import {HomeModule} from './components/home/home.module';
import {LoginComponent} from './components/front/login/login';
import {Page404Component} from './components/front/pages/page404';

const routes: Routes = [
    {path: '', loadChildren: () => HomeModule, canActivate: [AuthGuard]},
    {path: 'login', component: LoginComponent},
    {path: '**', component: Page404Component}
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true, preloadingStrategy: PreloadAllModules})],
    exports: [RouterModule]
})
export class AppRoutingModule {}