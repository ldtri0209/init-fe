import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NguiAutoCompleteModule} from '@ngui/auto-complete';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {BootstrapModalModule} from 'ng2-bootstrap-modal';

import {SidebarDirective, BreadcrumbDirective} from '../../_directives/index';
import {StatusOrderPipe, StatusPurchasePipe, SourceOrderPipe, PaymentMethodOrderPipe, TypeOrderPipe} from '../../_pipes/index';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        NguiAutoCompleteModule,
        CurrencyMaskModule,
        BootstrapModalModule.forRoot({container: document.body}),
    ],
    declarations: [
        SidebarDirective, BreadcrumbDirective,
        StatusOrderPipe, StatusPurchasePipe, SourceOrderPipe, PaymentMethodOrderPipe, TypeOrderPipe
    ],
    exports: [
        NguiAutoCompleteModule,
        CurrencyMaskModule,
        BootstrapModalModule,
        SidebarDirective, BreadcrumbDirective,
        StatusOrderPipe, StatusPurchasePipe, SourceOrderPipe, PaymentMethodOrderPipe, TypeOrderPipe
    ]
})
export class SharedModule {}