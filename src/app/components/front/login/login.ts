import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Http, Response} from '@angular/http';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '../../../_libraries/http.client';
import {Define} from '../../../_libraries/define';
import {CommonService} from '../../../services/common';
import {SaleService} from '../../../services/sale';
import {PurchaseService} from '../../../services/purchase';
declare const $: any;

@Component({
    templateUrl: 'login.html',
    providers: [CommonService, SaleService, PurchaseService]
})

export class LoginComponent implements OnInit {
    form: FormGroup;
    submitted: boolean = false;
    loading: boolean = false;
    err_mess: any = {};
    returnUrl: string;

    constructor(
        private _http: Http,
        private http: HttpClient,
        private route: ActivatedRoute,
        private router: Router,
        private commonService: CommonService,
        private saleService: SaleService,
        private purchaseService: PurchaseService
    ) {
        this.returnUrl = route.snapshot.queryParams['returnUrl'] || '/';
        if (localStorage.getItem('id_token')) {
            this.http.post('/auth/logout', null, null, false)
                .subscribe(
                    (res: Response | any) => {
                        localStorage.clear();
                    }
                );
        }
    }

    ngOnInit() {
        $('body').addClass('signwrapper');
        this.buildForm();
    }

    buildForm() {
        this.form = new FormGroup({
            user_name: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required])
        });
    }

    onSubmit() {
        this.submitted = true;
        if (this.form.valid) {
            this.loading = true;
            this._http.post(process.env.apiUrl + '/auth/login', this.form.value)
                .timeout(3000)
                .map((res: Response | any) => res.json())
                .catch(
                    (err: Response | any) => {
                        if (err.status === 0) {
                            return Observable.throw({
                                message: 'Network Error!!! Cannot connect to Server.'
                            });
                        }
                        return Observable.throw(err.json());
                    }
                )
                .subscribe(
                    (res: Response | any) => {
                        this.loading = false;
                        if (res.results.token) {
                            localStorage.setItem('id_token', res.results.token);
                            this.loadSettings();
                            $('body').removeClass('signwrapper');
                            this.router.navigateByUrl(this.returnUrl);
                        }
                    },
                    (err: Response | any) => {
                        this.loading = false;
                        this.err_mess = err;
                    }
                );
        }
    }

    loadSettings() {
        let flag: Boolean = false;
        Object.keys(localStorage).map((value:any, index:any) => {
            if (value.indexOf('settings.') >= 0) flag = true;
        });
        if (!flag) {
            this.commonService.getCategories()
                .subscribe(
                    (res: Response | any) => {
                        if (res && res !== 'undefined') {
                            localStorage.setItem('settings.categories', JSON.stringify(res));
                            Define.categories = res;
                        }
                    }
                );
            this.saleService.getConfig()
                .subscribe(
                    (res: Response | any) => {
                        Object.keys(res.results).map((key: any, index: any) => {
                            localStorage.setItem('settings.' + key, JSON.stringify(res.results[key]));
                            Define[key] = res.results[key];
                        })
                    }
                );
            this.purchaseService.getConfig()
                .subscribe(
                    (res: Response | any) => {
                        Object.keys(res.results).map((key: any, index: any) => {
                            localStorage.setItem('settings.' + key, JSON.stringify(res.results[key]));
                            Define[key] = res.results[key];
                        })
                    }
                );
        }
    }
}