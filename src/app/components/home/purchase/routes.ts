import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PurchaseComponent} from './component';
import {ListComponent} from './list/list';
import {ProductComponent} from './list/product';
import {AddComponent} from './action/add';
import {EditComponent} from './action/edit';
import {PickupComponent} from './calendar/pickup';

const routes: Routes = [
    {
        path: '',
        component: PurchaseComponent,
        data: {name: 'Purchase'},
        children: [
            {path: '', component: ListComponent, data: {name: 'List'}},
            {path: 'list', component: ListComponent, data: {name: 'List'}},
            {path: 'product', component: ProductComponent, data: {name: 'Product List'}},
            {path: 'add', component: AddComponent, data: {name: 'Create'}},
            {path: 'edit/:id', component: EditComponent, data: {name: 'Edit'}},
            {path: 'calendar/pickup', component: PickupComponent, data: {name: 'Pickup Scheduled'}}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PurchaseRoutingModule {}