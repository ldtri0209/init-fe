import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CurrencyPipe} from '@angular/common';
import {Define} from '../../../../_libraries/define';
import {Util} from '../../../../_libraries/util';
import {PurchaseService} from '../../../../services/purchase';
declare const $: any;
declare const moment: any;
declare const swal: any;

@Component({
    templateUrl: 'list.html',
    styles: ['.table > tfoot > tr > th { text-transform: none; }'],
    providers: [CurrencyPipe]
})

export class ListComponent implements OnInit {
    purchase_status: any = Define.purchase_status;

    filter: any = {};
    table: any;
    columns: Array<Object> = [
        { title: '', data: null },
        { title: 'ID', data: 'id', width: '30px' },
        { title: 'Created At', data: 'created_at', render: (data:any, type:any, row:any, meta:any) => {
            return moment(data).format('DD-MM-YYYY HH:mm');
        } },
        { title: 'Supplier', data: 'supplier.name' },
        { title: 'Total', data: 'total', render: (data:any, type:any, row:any, meta:any) => {
            return this.currency.transform(data, 'VND', true);
        } },
        { title: 'Status', data: 'status', render: (data:any, type:any, row:any, meta:any) => {
            return Define.transformStatusPurchase(data);
        } },
        { title: 'Created By', data: 'created_by.name' },
        { title: 'Updated At', data: 'updated_at', render: (data:any, type:any, row:any, meta:any) => {
            return moment(data).format('DD-MM-YYYY HH:mm');
        }, visible: false },
        { title: 'Updated By', data: 'updated_by.name', visible: false },
        { title: '', data: null, width: '31px' }
    ];

    constructor(
        private router: Router,
        private currency: CurrencyPipe,
        private service: PurchaseService
    ) {}

    ngOnInit() {
        $('.select2').select2({minimumResultsForSearch: Infinity, tags: true});
        $.fn.dataTable.ext.errMode = 'none';
        this.table = $('#data-table').DataTable({
            dom: "<'row'<'col-sm-6'l><'col-sm-6'C>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7 pagination-bordered'p>>",
            colReorder: true,
            stateSave: true,
            stateSaveCallback: function(settings:any, data:any) {
                localStorage.setItem('DataTables_' + settings.sInstance + '_purchase', JSON.stringify(data));
            },
            stateLoadCallback: function(settings:any) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance + '_purchase'));
            },
            stateLoadParams: (settings:any, data:any) => {
                data.search.search = '';
                data.columns.map((v:any,i:any) => {
                    v.search.search = '';
                });
            },
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            columns: this.columns,
            columnDefs: [
                {
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: ''
                },
                { targets: 1, responsivePriority: 1},
                {
                    targets: -1,
                    responsivePriority: 2,
                    orderable: false,
                    searchable: false,
                    render: function (data:any, type:any, row:any, meta:any) {
                        let html: string = '<ul class="table-options"><li><a class="edit" style="cursor: pointer"><i class="fa fa-pencil"></i></a></li>';

                        if (data.status === 'NW') {
                            html += '<li><a class="delete" style="cursor: pointer"><i class="fa fa-trash"></i></a></li>';
                        }

                        html += '</ul>';
                        return html;
                    }
                }
            ],
            order: [[1, 'desc']],
            serverSide: true,
            ajax: (data:any, callback:any, settings:any) => {
                let params = {};
                params['limit'] = data.length;
                params['page'] = (data.start/data.length)+1;
                params['order'] = {
                    data: (typeof data.columns[data.order[0].column].data === 'object') ? data.columns[data.order[0].column].data.filter : data.columns[data.order[0].column].data,
                    value: data.order[0].dir
                };
                params['columns'] = {};
                data.columns.map((value:any, index:any) => {
                    if (value.search.value) {
                        if (typeof value.data === 'object') value.data = value.data.filter;
                        if (value.data.split('.').length > 1) value.data = value.data.split('.').join('-');
                        params['columns'][value.data] = {
                            value: value.search.value,
                            regex: value.search.regex
                        };
                    }
                });
                this.filter = params;
                this.service.get(params)
                    .subscribe(
                        (res: Response | any) => {
                            let callbackData = {
                                data: res.results.data,
                                draw: data.draw,
                                recordsFiltered: res.results.total,
                                recordsTotal: res.results.total
                            };
                            callback(callbackData);
                        }
                    );
            },
            rowCallback: (row:any, data:any, index:any) => {
                setTimeout(() => {
                    $(row).find('td .edit').on('click', () => {
                        this.router.navigate(['/purchase/edit', data.id]);
                    });
                    $(row).find('td:last-child .delete').on('click', () => {
                        this.remove(data.id, row);
                    });
                    // $(row).find('td .log').on('click', () => {
                    //     this.router.navigate(['/product/edit', data.product.id]);
                    // });
                }, 1);
            },
            initComplete: (settings:any, json:any) => {
                $('input[type="daterange"]').daterangepicker({
                    autoApply: true,
                    autoUpdateInput: false,
                    showDropdowns: true,
                    linkedCalendars: false,
                    locale: {
                        format: 'DD-MM-YYYY',
                        separator: '|'
                    }
                }).on('apply.daterangepicker', function(event:any, picker:any) {
                    $(this).val(picker.startDate.format('DD-MM-YYYY') + '|' + picker.endDate.format('DD-MM-YYYY')).trigger('change');
                });
            },
            oLanguage: {
                oPaginate: {
                    sNext: '<i class="fa fa-angle-right"></i>',
                    sPrevious: '<i class="fa fa-angle-left"></i>'
                }
            },
            drawCallback: function () {
                $('.dataTables_paginate > .pagination').addClass('pagination-bordered');
            }
        }).columns().every(function() {
            let column = this;
            $('input', column.footer()).on('change', function() {
                if (column.search() !== this.value) {
                    let type = $(this).data('type');
                    let value: any = [];
                    this.value.split('|').map((v: any, i: any) => {
                        if (type === 'date') {
                            if (i === 0) v = moment.utc(v, 'DD-MM-YYYY').hour(0).minute(0).second(0).toISOString();
                            if (i === 1) v = moment.utc(v, 'DD-MM-YYYY').hour(23).minute(59).second(59).toISOString();
                        }
                        value.push(v);
                    });
                    column.search(value.join('|'), type === 'regex').draw();
                }
            });
            $('.select2', this.footer()).select2().on('select2:select select2:unselect', function (e:any) {
                let data = $(this).select2('data');
                let temp: any = [];
                data.map((value:any, index:any) => {
                    temp.push(value.id);
                });
                column.search(temp).draw();
            });
        });
    }

    export() {
        delete this.filter.limit;
        this.service.get(this.filter)
            .subscribe(
                (res: Response | any) => {
                    Util.exportAsExcelFile(res.results.data, this.columns, 'purchase');
                }
            );
    }

    remove(id:Number, target:any) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(() => {
            this.service.removeById(id)
                .subscribe(
                    (res: Response | any) => {
                        $(target).remove();
                        this.table.draw();
                    }
                );
        }, () => {});
    }
}