import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/shared.module';

import {PurchaseRoutingModule} from './routes';

import {PurchaseComponent} from './component';
import {ListComponent} from './list/list';
import {ProductComponent} from './list/product';
import {AddComponent} from './action/add';
import {EditComponent} from './action/edit';
import {PickupComponent} from './calendar/pickup';
import {SpecModal} from './modal/spec';
import {DetailModal} from './modal/detail';

import {PurchaseService} from '../../../services/purchase';

@NgModule({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        SharedModule,
        PurchaseRoutingModule
    ],
    declarations: [
        PurchaseComponent,
        ListComponent,
        ProductComponent,
        AddComponent,
        EditComponent,
        PickupComponent,
        SpecModal,
        DetailModal
    ],
    entryComponents: [
        SpecModal,
        DetailModal
    ],
    providers: [PurchaseService]
})
export class PurchaseModule {}