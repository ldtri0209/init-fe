import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CurrencyPipe} from '@angular/common';
import {DialogService} from 'ng2-bootstrap-modal';
import {Define} from '../../../../_libraries/define';
import {Util} from '../../../../_libraries/util';
import {PurchaseService} from '../../../../services/purchase';
import {DetailModal} from '../modal/detail';
declare const $: any;
declare const moment: any;
declare const swal: any;

@Component({
    templateUrl: 'pickup.html',
    providers: [CurrencyPipe]
})

export class PickupComponent implements OnInit {
    constructor(
        private router: Router,
        private currency: CurrencyPipe,
        private dialog: DialogService,
        private service: PurchaseService
    ) {}

    ngOnInit() {
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            editable: true,
            displayEventTime : false,
            eventLimit: true,
            scrollTime: '00:00:00',
            events: (start:any, end:any, timezone:any, callback:any) => {
                let params = {};
                params['columns'] = {};
                params['columns']['scheduled_date'] = {
                    value: moment.utc(start).toISOString() + '|' + moment.utc(end).toISOString(),
                    regex: false
                };
                params['columns']['purchase-status'] = {
                    value: 'CO',
                    regex: false
                };
                this.service.getProduct(params)
                    .subscribe(
                        (res: Response | any) => {
                            let events:any = [];
                            res.results.data.map((value:any, index:any) => {
                                value.prod_spec = JSON.parse(value.prod_spec);
                                let options = {
                                    title: value.prod_name + '\n' + value.purchase.supplier.name,
                                    start: value.scheduled_date,
                                    data: value
                                };
                                if (value.status === 'DO') options['color'] = '#6eba57';
                                events.push(options);
                            });
                            callback(events);
                        }
                    )
            },
            eventClick: (calEvent:any, jsEvent:any, view:any) => {
                if ($(jsEvent.target).attr('class') === 'fc-content') {
                    $(jsEvent.target).parent().css('border-color', 'red');
                } else {
                    $(jsEvent.target).parent().parent().css('border-color', 'red');
                }
                this.showModalDetail(calEvent.data, jsEvent.target)
                    .subscribe(
                        (res:any) => {
                            console.log(res);
                            if (res) {
                                Object.assign(calEvent.data, res);
                            }
                        }
                    );
            },
            eventDrop: (event:any, delta:any, revertFunc:any) => {
                swal({
                    title: 'Are you sure?',
                    // text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, move it!'
                }).then(
                    (res:any) => {
                        if (res) {
                            let params = {
                                scheduled_date: moment.utc(event.start).toISOString()
                            };
                            this.service.updateByDetailId(event.data.id, params)
                                .subscribe(
                                    (res: Response | any) => {
                                        swal(
                                            'Moved!',
                                            'Moved to ' + event.start.format('DD-MM-YYYY'),
                                            'success'
                                        )
                                    }
                                );
                        }
                    }
                ).catch(
                    (res:any) => {
                        revertFunc();
                    }
                );
            }
        });
    }

    showModalDetail(data:any, target:any) {
        return this.dialog.addDialog(DetailModal, {
            title: data.prod_name,
            data: data,
            target: target
        }, {
            closeByClickingOutside: true
        });
    }
}