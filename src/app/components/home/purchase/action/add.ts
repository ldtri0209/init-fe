import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {FormGroup, FormControl, Validators, FormArray} from '@angular/forms';
import {DialogService} from 'ng2-bootstrap-modal';
import {Define} from '../../../../_libraries/define';
import {PurchaseService} from '../../../../services/purchase';
import {CommonService} from '../../../../services/common';
import {SpecModal} from '../modal/spec';
declare const $: any;
declare const moment: any;

@Component({
    templateUrl: 'add.html',
    providers: [CommonService]
})

export class AddComponent implements OnInit {
    suppliers: any = Define.suppliers;
    supplier: Object = {};

    form: FormGroup;
    submitted: boolean = false;

    constructor(
        private router: Router,
        private location: Location,
        private dialog: DialogService,
        private service: PurchaseService,
        private commonService: CommonService
    ) {
        this.buildForm();
    }

    ngOnInit() {
        let that = this;

        $('.select2').select2({
            minimumResultsForSearch: Infinity
        }).on('change', function () {
            that.form.controls['attributes'].setValue($(this).val());
        });

        $('#suppliers').select2().on('change', function() {
            that.form.controls['supplier_id'].setValue($(this).val());
        });
    }

    buildForm() {
        this.form = new FormGroup({
            supplier_id: new FormControl('', [Validators.required]),
            total: new FormControl(0),
            products: new FormArray([]),
        });
    }

    onSubmit() {
        this.submitted = true;
        if (this.form.valid) {
            let total = 0;
            this.form.value.products.map((value:any, index:any) => {
                value.scheduled_date = moment.utc(value.scheduled_date, 'DD-MM-YYYY').toISOString();
                total += value.prod_price * value.qty;
            });
            this.form.value.total = total;
            this.service.create(this.form.value)
                .subscribe(
                    (res: Response | any) => {
                        this.router.navigate(['/purchase']);
                    }
                );
        }
    }

    back() {
        this.location.back();
    }

    loadProducts(keyword?: String) {
        let params = {
            'limit': 20
        };
        if (keyword) {
            params['searchString'] = keyword;
        }
        return this.commonService.getProducts(params);
    }

    selectedItem(value:any) {
        let attributes:any = [];
        value.attributes.map((v:any, i:any) => {
            let check:any = Define.transformCateAttribute(value.supplierCateId, v.code);
            if (check.is_visible) {
                attributes.push(v);
            }
            v.value = '';
        });

        if (!attributes.length) {
            value.attributes.map((v:any, i:any) => {
                let check:any = Define.transformCateAttribute(0, v.code);
                if (check.is_visible) {
                    attributes.push(v);
                }
                v.value = '';
            });
        }

        let control = <FormArray>this.form.controls['products'];
        control.push(
            new FormGroup({
                prod_image: new FormControl(value.image[0]),
                prod_name: new FormControl(value.name),
                sku: new FormControl(value.sku),
                prod_spec: new FormControl(attributes),
                prod_price: new FormControl(0, [Validators.required, Validators.min(1)]),
                qty: new FormControl(0, [Validators.required, Validators.min(1)]),
                total: new FormControl({value: 0, disabled: true}),
                scheduled_date: new FormControl('', [Validators.required]),
                reference: new FormControl('', [Validators.required]),
                note: new FormControl('')
            })
        );
        setTimeout(() => {
            let that = this;
            $('input[type=datetime]').datepicker({
                dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true,
                yearRange: '-0:+50',
                onSelect: function (text: String) {
                    let target = $(this).attr('id');
                    let control = $(this).attr('formControlName');
                    let value = {};
                    value[control] = text;
                    (<FormArray>that.form.controls['products']).controls[target].patchValue(value);
                }
            });
        }, 1);
    }

    selectedSupplierItem(value:any) {
        this.form.controls.supplier_id.setValue(value.id);
        this.supplier = value;
    }

    clearInput(event:any) {
        event.target.value = '';
    }

    remove(target:any) {
        const control = <FormArray>this.form.controls['products'];
        control.removeAt(target);
    }

    calculateTotal(target:any) {
        (<FormArray>this.form.controls['products']).controls.map((product:any, index:any) => {
            if (index === target) {
                product.patchValue({total: product.value.prod_price * product.value.qty});
            }
        })
    }

    showModalSpec(data:any) {
        return this.dialog.addDialog(SpecModal, {
            title: data.prod_name,
            data: data
        }, {
            closeByClickingOutside: true
        });
    }

    transformSpecificationValue(data:any, value:any) {
        let label = value;
        if (data.type === 'select' || data.type === 'multiselect' || data.type === 'boolean') {
            data.optionData.map((v: any, i: any) => {
                if (v.value == value) {
                    label = v.label;
                }
            });
        }
        let result = '';
        if (data.type === 'image' || data.type === 'media_image' || data.type === 'gallery') {
            result = '<img src="' + value + '" width="80">';
        } else {
            result = label;
        }
        return result;
    }
}