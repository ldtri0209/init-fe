import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {FormGroup, FormControl, Validators, FormArray} from '@angular/forms';
import {DialogService} from 'ng2-bootstrap-modal';
import {Define} from '../../../../_libraries/define';
import {PurchaseService} from '../../../../services/purchase';
import {CommonService} from '../../../../services/common';
import {SpecModal} from '../modal/spec';
declare const $: any;
declare const moment: any;
declare const swal: any;

@Component({
    templateUrl: 'edit.html',
    styleUrls: ['edit.scss'],
    providers: [CommonService]
})

export class EditComponent implements OnInit {
    suppliers: any = Define.suppliers;

    form: FormGroup;
    submitted: boolean = false;
    params: any = {};
    data: any = {
        id: '',
        created_at: '',
        created_by: {
            name: ''
        },
        updated_at: '',
        updated_by: {
            name: ''
        },
    };

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private dialog: DialogService,
        private service: PurchaseService,
        private commonService: CommonService
    ) {
        this.buildForm();
        route.params.subscribe(params => this.params = params);
        this.loadData();
    }

    ngOnInit() {
        let that = this;
        $('.select2').select2({
            minimumResultsForSearch: Infinity,
            tags: true
        }).on('change', function () {
            that.form.controls['attributes'].setValue($(this).val());
        });
    }

    buildForm() {
        this.form = new FormGroup({
            supplier_id: new FormControl('', [Validators.required]),
            total: new FormControl(0),
            products: new FormArray([]),
        });
    }

    loadData() {
        this.service.getById(this.params['id'])
            .subscribe(
                (res: Response | any) => {
                    this.data = res.results;
                    this.form.patchValue({
                        supplier_id: res.results.supplier_id,
                        total: res.results.total
                    });
                    res.results.products.map((value:any, index:any) => {
                        (<FormArray>this.form.controls['products']).setControl(index,
                            new FormGroup({
                                id: new FormControl(value.id),
                                received_qty: new FormControl(value.received_qty),
                                prod_image: new FormControl(value.prod_image),
                                prod_name: new FormControl(value.prod_name),
                                sku: new FormControl(value.sku),
                                prod_spec: new FormControl(JSON.parse(value.prod_spec)),
                                prod_price: new FormControl(value.prod_price, [Validators.required, Validators.min(1)]),
                                qty: new FormControl(value.qty, [Validators.required, Validators.min(1)]),
                                total: new FormControl({value: value.prod_price * value.qty, disabled: true}),
                                scheduled_date: new FormControl(moment(value.scheduled_date).format('DD-MM-YYYY'), [Validators.required]),
                                reference: new FormControl(value.reference, [Validators.required]),
                                note: new FormControl(value.note),
                                status: new FormControl(value.status)
                            })
                        );
                    });
                    setTimeout(() => {
                        let that = this;
                        $('input[type=datetime]').datepicker({
                            dateFormat: 'dd-mm-yy',
                            changeMonth: true,
                            changeYear: true,
                            yearRange: '-0:+50',
                            onSelect: function (text: String) {
                                let target = $(this).attr('id');
                                let control = $(this).attr('formControlName');
                                let value = {};
                                value[control] = text;
                                (<FormArray>that.form.controls['products']).controls[target].patchValue(value);
                            }
                        });
                    }, 1);
                }
            );
    }

    onSubmit(status?: String) {
        this.submitted = true;
        if (this.form.valid) {
            let total = 0;
            let params = JSON.parse(JSON.stringify(this.form.value));
            params.products.map((value:any, index:any) => {
                value.scheduled_date = moment.utc(value.scheduled_date, 'DD-MM-YYYY').toISOString();
                total += value.prod_price * value.qty;
            });
            params.total = total;
            if (typeof status) params['status'] = status;
            this.service.updateById(this.params['id'], params)
                .subscribe(
                    (res: Response | any) => {
                        if (status === 'SM') swal('Success', 'Sent mail to Supplier successfully', 'success');
                        this.data = res.results;
                    }
                );
        }
    }

    back() {
        this.location.back();
    }

    loadProducts(keyword?: String) {
        let params = {
            'limit': 20
        };
        if (keyword) {
            params['searchString'] = keyword;
        }
        return this.commonService.getProducts(params);
    }

    selectedItem(value:any) {
        let attributes:any = [];
        value.attributes.map((v:any, i:any) => {
            let check:any = Define.transformCateAttribute(value.supplierCateId, v.code);
            if (check.is_visible) {
                attributes.push(v);
            }
        });
        let control = <FormArray>this.form.controls['products'];
        control.push(
            new FormGroup({
                prod_image: new FormControl(value.image[0]),
                prod_name: new FormControl(value.name),
                sku: new FormControl(value.sku),
                prod_spec: new FormControl(attributes),
                prod_price: new FormControl(0, [Validators.required, Validators.min(1)]),
                qty: new FormControl(0, [Validators.required, Validators.min(1)]),
                total: new FormControl({value: 0, disabled: true}),
                scheduled_date: new FormControl('', [Validators.required]),
                reference: new FormControl('', [Validators.required]),
                note: new FormControl('')
            })
        );
        setTimeout(() => {
            let that = this;
            $('input[type=datetime]').datepicker({
                dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true,
                yearRange: '-0:+50',
                onSelect: function (text: String) {
                    let target = $(this).attr('id');
                    let control = $(this).attr('formControlName');
                    let value = {};
                    value[control] = text;
                    (<FormArray>that.form.controls['products']).controls[target].patchValue(value);
                }
            });
        }, 1);
    }

    clearInput(event:any) {
        event.target.value = '';
    }

    remove(target:any) {
        const control = <FormArray>this.form.controls['products'];
        control.removeAt(target);
    }

    receive(target:any) {
        let data = (<FormArray>this.form.controls['products']).controls[target];
        let params = {
            received_qty: data.value.qty
        };
        this.service.updateByDetailId(data.value.id, params)
            .subscribe(
                (res: Response | any) => {
                    swal('Success', 'Received all products successfully', 'success');
                    data.patchValue({
                        received_qty: res.results.received_qty
                    });
                }
            )
    }

    calculateTotal(target:any) {
        (<FormArray>this.form.controls['products']).controls.map((product:any, index:any) => {
            if (index === target) {
                product.patchValue({total: product.value.prod_price * product.value.qty});
            }
        })
    }

    showModalSpec(data:any) {
        return this.dialog.addDialog(SpecModal, {
            title: data.prod_name,
            data: data
        }, {
            closeByClickingOutside: true
        });
    }

    transformSpecificationValue(data:any, value:any) {
        let label = value;
        if (data.type === 'select' || data.type === 'multiselect' || data.type === 'boolean') {
            data.optionData.map((v: any, i: any) => {
                if (v.value == value) {
                    label = v.label;
                }
            });
        }
        let result = '';
        if (data.type === 'image' || data.type === 'media_image' || data.type === 'gallery') {
            result = '<img src="' + value + '" width="80">';
        } else {
            result = label;
        }
        return result;
    }
}