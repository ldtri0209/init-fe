import {Component, OnInit} from '@angular/core';
import {DialogComponent, DialogService} from 'ng2-bootstrap-modal';
import {PurchaseService} from '../../../../services/purchase';
declare const $: any;
declare const swal: any;

export interface Model {
    title: string;
    data: any;
    target: any;
}
@Component({
    templateUrl: 'detail.html',
    styleUrls: ['detail.scss']
})
export class DetailModal extends DialogComponent<Model, boolean> implements Model, OnInit {
    title: string;
    data: any;
    target: any;

    constructor(
        public dialog: DialogService,
        private service: PurchaseService
    ) {
        super(dialog);
    }

    ngOnInit() {
        document.getElementsByTagName('body')[0].classList.add('modal-open');
    }

    ngOnDestroy() {
        document.getElementsByTagName('body')[0].classList.remove('modal-open');
        if ($(this.target).attr('class') === 'fc-content') {
            $(this.target).parent().css('border-color', 'transparent');
        } else {
            $(this.target).parent().parent().css('border-color', 'transparent');
        }
    }

    transformSpecificationValue(data:any, value:any) {
        let label = value;
        if (data.type === 'select' || data.type === 'multiselect' || data.type === 'boolean') {
            data.optionData.map((v: any, i: any) => {
                if (v.value == value) {
                    label = v.label;
                }
            });
        }
        let result = '';
        if (data.type === 'image' || data.type === 'media_image' || data.type === 'gallery') {
            result = '<img src="' + value + '" width="80">';
        } else {
            result = label;
        }
        return result;
    }

    pickUp() {
        let params = {
            received_qty: this.data.qty,
            status: 'DO'
        };
        this.service.updateByDetailId(this.data.id, params)
            .subscribe(
                (res: Response | any) => {
                    swal(
                        'Received!',
                        'Received ' + this.data.qty + ' items',
                        'success'
                    );
                    this.result = res.results;
                    this.close();
                }
            );
    }
}