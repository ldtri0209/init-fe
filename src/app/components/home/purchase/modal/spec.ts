import {Component, OnInit} from '@angular/core';
import {DialogComponent, DialogService} from 'ng2-bootstrap-modal';
declare const $: any;

export interface Model {
    title: string;
    data: any;
    input: any;
}
@Component({
    templateUrl: 'spec.html'
})
export class SpecModal extends DialogComponent<Model, boolean> implements Model, OnInit {
    title: string;
    data: any;
    input: any = {};

    constructor(
        public dialog: DialogService
    ) {
        super(dialog);
    }

    ngOnInit() {
        document.getElementsByTagName('body')[0].classList.add('modal-open');
        let that = this;
        setTimeout(() => {
            $('input[type=datetime]').datepicker({
                dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true,
                yearRange: '-0:+50',
                onSelect: function (text: String) {
                    // let target = $(this).attr('id');
                    // let control = $(this).attr('formControlName');
                    // let value = {};
                    // value[control] = text;
                    // (<FormArray>that.form.controls['products']).controls[target].patchValue(value);
                }
            });
            $('.select2').select2().on('change', function () {
                // that.form.controls['attributes'].setValue($(this).val());
            });
        }, 1);
    }

    ngOnDestroy(){
        document.getElementsByTagName('body')[0].classList.remove('modal-open');
    }

    selectImage(event:any, target:any) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = () => {
                this.data.prod_spec[target].value = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        }
    }
}