import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRouteSnapshot} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {JwtHelper} from 'angular2-jwt';
declare const $: any;

@Component({
    templateUrl: 'home.html'
})
export class HomeComponent implements OnInit {
    jwtHelper: JwtHelper = new JwtHelper();
    title: string;
    profile: any = {};
    currentTitle: string;

    constructor(
        private router: Router,
        private titleService: Title
    ) {
        this.profile = this.jwtHelper.decodeToken(localStorage.getItem('id_token')).data;
        if (this.profile) localStorage.setItem('profile', JSON.stringify(this.profile));
        this.currentTitle = titleService.getTitle();
        router.events.subscribe(() => {
            this.parseRoute(router.routerState.snapshot.root);
        });
    }

    ngOnInit() {
        $.getScript('../../../assets/js/quirk.js');
    }

    parseRoute(node: ActivatedRouteSnapshot) {
        if (node.data['name']) {
            this.title = node.data['name'];
            this.titleService.setTitle(this.currentTitle + ' - ' + node.data['name'].toUpperCase());
        }
        if (node.firstChild) {
            this.parseRoute(node.firstChild);
        }
    }
}