import {Component} from '@angular/core';
import {Response} from '@angular/http';
import {Define} from '../../../_libraries/define';
import {CommonService} from '../../../services/common';
import {SaleService} from '../../../services/sale';
import {PurchaseService} from '../../../services/purchase';
declare const $: any;

@Component({
    templateUrl: 'setting.html',
    providers: [CommonService, SaleService, PurchaseService]
})
export class SettingComponent {
    constructor(
        private commonService: CommonService,
        private saleService: SaleService,
        private purchaseService: PurchaseService
    ) {}

    reloadSettings() {
        this.commonService.getCategories()
            .subscribe(
                (res: Response | any) => {
                    if (res && res !== 'undefined') {
                        localStorage.setItem('settings.categories', JSON.stringify(res));
                        Define.categories = res;
                    }
                }
            );
        this.saleService.getConfig()
            .subscribe(
                (res: Response | any) => {
                    Object.keys(res.results).map((key:any, index:any) => {
                        localStorage.setItem('settings.' + key, JSON.stringify(res.results[key]));
                        Define[key] = res.results[key];
                    })
                }
            );
        this.purchaseService.getConfig()
            .subscribe(
                (res: Response | any) => {
                    Object.keys(res.results).map((key: any, index: any) => {
                        localStorage.setItem('settings.' + key, JSON.stringify(res.results[key]));
                        Define[key] = res.results[key];
                    })
                }
            );
    }
}