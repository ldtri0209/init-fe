import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/shared.module';

import {StockRoutingModule} from './routes';

import {StockComponent} from './component';
import {ListComponent} from './list/list';
import {AddComponent} from './action/add';
import {EditComponent} from './action/edit';
import {LogModal} from './modal/log';

import {StockService} from '../../../services/stock';

@NgModule({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        SharedModule,
        StockRoutingModule
    ],
    declarations: [
        StockComponent,
        ListComponent,
        AddComponent,
        EditComponent,
        LogModal
    ],
    entryComponents: [
        LogModal
    ],
    providers: [StockService]
})
export class StockModule {}