import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Util} from '../../../../_libraries/util';
import {StockService} from '../../../../services/stock';
import {LogModal} from '../modal/log';
import {DialogService} from 'ng2-bootstrap-modal';
declare const $: any;
declare const moment: any;
declare const swal: any;

@Component({
    templateUrl: 'list.html',
    styles: ['.table > tfoot > tr > th { text-transform: none; }']
})

export class ListComponent implements OnInit {
    filter: any = {};
    table: any;
    columns: Array<Object> = [
        { title: '', data: null },
        { title: 'ID', data: 'id', width: '30px' },
        { title: 'Name', data: 'name' },
        { title: 'SKU', data: 'sku' },
        { title: 'Supplier SKU', data: 'supplier_sku' },
        { title: 'Stock', data: 'stock' },
        { title: 'Reserved', data: 'reserved' },
        { title: 'Available', data: null, render: (data:any, type:any, row:any, meta:any) => {
            return row.stock - row.reserved;
        }, sortable: false },
        { title: 'Backable', data: 'back_order', width: '25px', className: 'text-center', render: (data:any, type:any, row:any, meta:any) => {
            let status: String;
            switch (data) {
                case 0: status = 'false'; break;
                case 1: status = 'true'; break;
            }
            return '<div class="toggle-wrapper"><div class="toggle toggle-light success" data-toggle-on="' + status + '"></div></div>';
        } },
        { title: 'Created At', data: 'created_at', render: (data:any, type:any, row:any, meta:any) => {
            return moment(data).format('DD-MM-YYYY HH:mm');
        } },
        { title: '', data: null, width: '31px' }
    ];

    constructor(
        private router: Router,
        private dialog: DialogService,
        private service: StockService
    ) {}

    ngOnInit() {
        $('.select2').select2({minimumResultsForSearch: Infinity, tags: true});
        $.fn.dataTable.ext.errMode = 'none';
        this.table = $('#data-table').DataTable({
            dom: "<'row'<'col-sm-6'l><'col-sm-6'C>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            colReorder: true,
            stateSave: true,
            stateSaveCallback: function(settings:any, data:any) {
                localStorage.setItem('DataTables_' + settings.sInstance + '_stock', JSON.stringify(data));
            },
            stateLoadCallback: function(settings:any) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance + '_stock'));
            },
            stateLoadParams: (settings:any, data:any) => {
                data.search.search = '';
                data.columns.map((v:any,i:any) => {
                    v.search.search = '';
                });
            },
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            columns: this.columns,
            columnDefs: [
                {
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: ''
                },
                { targets: 1, responsivePriority: 1},
                {
                    targets: -1,
                    responsivePriority: 2,
                    orderable: false,
                    searchable: false,
                    render: function (data:any, type:any, row:any, meta:any) {
                        return '<ul class="table-options"><li><a class="log" style="cursor: pointer"><i class="fa fa-history"></i></a></li></ul>';
                    }
                }
            ],
            order: [[1, 'desc']],
            serverSide: true,
            ajax: (data:any, callback:any, settings:any) => {
                let params = {};
                params['limit'] = data.length;
                params['page'] = (data.start/data.length)+1;
                params['order'] = {
                    data: (typeof data.columns[data.order[0].column].data === 'object') ? data.columns[data.order[0].column].data.filter : data.columns[data.order[0].column].data,
                    value: data.order[0].dir
                };
                params['columns'] = {};
                data.columns.map((value:any, index:any) => {
                    if (value.search.value) {
                        if (typeof value.data === 'object') value.data = value.data.filter;
                        if (value.data.split('.').length > 1) value.data = value.data.split('.').join('-');
                        params['columns'][value.data] = {
                            value: value.search.value,
                            regex: value.search.regex
                        };
                    }
                });
                this.filter = params;
                this.service.get(params)
                    .subscribe(
                        (res: Response | any) => {
                            let callbackData = {
                                data: res.results.data,
                                draw: data.draw,
                                recordsFiltered: res.results.total,
                                recordsTotal: res.results.total
                            };
                            callback(callbackData);
                        }
                    );
            },
            rowCallback: (row:any, data:any, index:any) => {
                setTimeout(() => {
                    $('.toggle').toggles();
                    // $(row).find('td .edit').on('click', () => {
                    //     this.router.navigate(['/product/edit', data.product.id]);
                    // });
                    $(row).find('td .log').on('click', () => {
                        this.showLog(data);
                        // this.router.navigate(['/product/edit', data.product.id]);
                    });
                }, 1);
            },
            initComplete: (settings:any, json:any) => {
                $('input[type="daterange"]').daterangepicker({
                    autoApply: true,
                    autoUpdateInput: false,
                    showDropdowns: true,
                    linkedCalendars: false,
                    locale: {
                        format: 'DD-MM-YYYY',
                        separator: '|'
                    }
                }).on('apply.daterangepicker', function(event:any, picker:any) {
                    $(this).val(picker.startDate.format('DD-MM-YYYY') + '|' + picker.endDate.format('DD-MM-YYYY')).trigger('change');
                });
            },
            oLanguage: {
                oPaginate: {
                    sNext: '<i class="fa fa-angle-right"></i>',
                    sPrevious: '<i class="fa fa-angle-left"></i>'
                }
            },
            drawCallback: function () {
                $('.dataTables_paginate > .pagination').addClass('pagination-bordered');
            }
        }).columns().every(function() {
            let column = this;
            $('input', column.footer()).on('change', function() {
                if (column.search() !== this.value) {
                    let type = $(this).data('type');
                    let value: any = [];
                    this.value.split('|').map((v: any, i: any) => {
                        if (type === 'date') {
                            if (i === 0) v = moment.utc(v, 'DD-MM-YYYY').hour(0).minute(0).second(0).toISOString();
                            if (i === 1) v = moment.utc(v, 'DD-MM-YYYY').hour(23).minute(59).second(59).toISOString();
                        }
                        value.push(v);
                    });
                    column.search(value.join('|'), type === 'regex').draw();
                }
            });
            $('.select2', this.footer()).select2().on('select2:select select2:unselect', function (e:any) {
                let data = $(this).select2('data');
                let temp: any = [];
                data.map((value:any, index:any) => {
                    temp.push(value.id);
                });
                column.search(temp).draw();
            });
        });
    }

    export() {
        delete this.filter.limit;
        this.service.get(this.filter)
            .subscribe(
                (res: Response | any) => {
                    Util.exportAsExcelFile(res.results.data, this.columns, 'stock');
                }
            );
    }

    showLog(data:any) {
        return this.dialog.addDialog(LogModal, {
            title: data.name,
            data: data.logs
        }, {
            closeByClickingOutside: true
        });
    }
}