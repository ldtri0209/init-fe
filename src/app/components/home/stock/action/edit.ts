import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Define} from '../../../../_libraries/define';
import {StockService} from '../../../../services/stock';
declare const $: any;

@Component({
    templateUrl: 'edit.html'
})

export class EditComponent implements OnInit {
    form: FormGroup;
    submitted: boolean = false;
    params: any = {};

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private service: StockService
    ) {
        this.buildForm();
        route.params.subscribe(params => this.params = params);
        this.loadData();
    }

    ngOnInit() {
        let that = this;
        $('.select2').select2({
            minimumResultsForSearch: Infinity,
            tags: true
        }).on('change', function () {
            that.form.controls['attributes'].setValue($(this).val());
        });
    }

    buildForm() {
        this.form = new FormGroup({
            name: new FormControl('', [Validators.required]),
            sku: new FormControl('', [Validators.required]),
            desc: new FormControl(''),
            attributes: new FormControl([], [Validators.required]),
            company_id: new FormControl('', [Validators.required]),
            unit: new FormControl('', [Validators.required]),
            cate: new FormControl('', [Validators.required]),
            stock_type: new FormControl('', [Validators.required]),
            status: new FormControl('AC', [Validators.required])
        });
    }

    loadData() {
        // this.service.getById(this.params['id'])
        //     .subscribe(
        //         (res: Response | any) => {
        //             let attributes:any = [];
        //             res.results.data.attrs.map((value:any, index:any) => {
        //                 attributes.push(value.attribute.name);
        //             });
        //             $('.select2').val(attributes).change();
        //             this.form.patchValue(res.results.data);
        //         }
        //     );
    }

    onSubmit() {
        this.submitted = true;
        if (this.form.valid) {
            this.form.value.product_id = this.params['id'];
            // this.service.updateById(this.form.value)
            //     .subscribe(
            //         (res: Response | any) => {
            //             this.router.navigate(['/product']);
            //         }
            //     );
        }
    }

    back() {
        this.location.back();
    }
}