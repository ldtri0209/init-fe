import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Define} from '../../../../_libraries/define';
import {StockService} from '../../../../services/stock';
declare const $: any;

@Component({
    templateUrl: 'add.html'
})

export class AddComponent implements OnInit {
    form: FormGroup;
    submitted: boolean = false;

    constructor(
        private router: Router,
        private location: Location,
        private service: StockService
    ) {
        this.buildForm();
    }

    ngOnInit() {
        let that = this;
        $('.select2').select2({
            minimumResultsForSearch: Infinity,
            tags: true
        }).on('change', function () {
            that.form.controls['attributes'].setValue($(this).val());
        });
    }

    buildForm() {
        this.form = new FormGroup({
            name: new FormControl('', [Validators.required]),
            sku: new FormControl('', [Validators.required]),
            desc: new FormControl(''),
            attributes: new FormControl([], [Validators.required]),
            company_id: new FormControl(1, [Validators.required]),
            unit: new FormControl('', [Validators.required]),
            cate: new FormControl('', [Validators.required]),
            stock_type: new FormControl('', [Validators.required]),
            status: new FormControl('AC', [Validators.required])
        });
    }

    onSubmit() {
        this.submitted = true;
        if (this.form.valid) {
            // this.service.create(this.form.value)
            //     .subscribe(
            //         (res: Response | any) => {
            //             this.router.navigate(['/stock']);
            //         }
            //     );
        }
    }

    back() {
        this.location.back();
    }
}