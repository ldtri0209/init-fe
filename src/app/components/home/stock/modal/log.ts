import {Component, OnInit} from '@angular/core';
import {DialogComponent, DialogService} from 'ng2-bootstrap-modal';
import {Define} from '../../../../_libraries/define';
declare const $: any;
declare const moment: any;

export interface Model {
    title: string;
    data: any;
}
@Component({
    templateUrl: 'log.html'
})
export class LogModal extends DialogComponent<Model, boolean> implements Model, OnInit {
    title: string;
    data: any;

    columns: Array<Object> = [
        { title: '', data: null },
        { title: 'ID', data: 'id' },
        { title: 'Action', data: 'type', render: (data:any, type:any, row:any, meta:any) => {
            return Define.transformStockLogType(data);
        } },
        { title: 'Content', data: 'content' },
        { title: 'Value', data: 'value' },
        { title: 'Created At', data: 'created_at', render: (data:any, type:any, row:any, meta:any) => {
            return moment(data).format('DD-MM-YYYY HH:mm');
        } },
        { title: 'Created By', data: 'user.name' },
    ];

    constructor(
        public dialogService: DialogService
    ) {
        super(dialogService);
    }

    ngOnInit() {
        document.getElementsByTagName('body')[0].classList.add('modal-open');
        $('#log').DataTable({
            dom: "<'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            colReorder: true,
            stateSave: true,
            stateSaveCallback: function (settings: any, data: any) {
                localStorage.setItem('DataTables_' + settings.sInstance + '_stock', JSON.stringify(data));
            },
            stateLoadCallback: function (settings: any) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance + '_stock'));
            },
            stateLoadParams: (settings: any, data: any) => {
                data.search.search = '';
                data.columns.map((v: any, i: any) => {
                    v.search.search = '';
                });
            },
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            columns: this.columns,
            columnDefs: [
                {
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: ''
                },
                {targets: 1, responsivePriority: 1}
            ],
            order: [[1, 'desc']],
            data: this.data,
            oLanguage: {
                oPaginate: {
                    sNext: '<i class="fa fa-angle-right"></i>',
                    sPrevious: '<i class="fa fa-angle-left"></i>'
                }
            },
            drawCallback: function () {
                $('.dataTables_paginate > .pagination').addClass('pagination-bordered');
            }
        });
    }

    ngOnDestroy(){
        document.getElementsByTagName('body')[0].classList.remove('modal-open');
    }
}