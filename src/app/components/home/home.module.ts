import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SharedModule} from '../shared/shared.module';
import {HomeRoutingModule} from './home.routes';

import {HomeComponent} from './home.component';
import {DashboardComponent} from './dashboard/dashboard';
import {SettingComponent} from './setting/setting';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        HomeRoutingModule
    ],
    declarations: [
        HomeComponent,
        DashboardComponent,
        SettingComponent
    ]
})
export class HomeModule {}