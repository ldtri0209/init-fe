import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {HomeComponent} from './home.component';
import {DashboardComponent} from './dashboard/dashboard';
import {SettingComponent} from './setting/setting';
import {SaleModule} from './sale/module';
import {StockModule} from './stock/module';
import {PurchaseModule} from './purchase/module';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
            {path: 'dashboard', component: DashboardComponent, data: {name: 'Dashboard'}},
            {path: 'setting', component: SettingComponent, data: {name: 'Settings'}},
            {path: 'sale', loadChildren: () => SaleModule},
            {path: 'stock', loadChildren: () => StockModule},
            {path: 'purchase', loadChildren: () => PurchaseModule},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HomeRoutingModule {}