import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SaleComponent} from './component';
import {OrderModule} from './order/module';
import {ProductModule} from './product/module';

const routes: Routes = [
    {
        path: '',
        component: SaleComponent,
        data: {name: 'Sale'},
        children: [
            {path: 'order', loadChildren: () => OrderModule},
            {path: 'product', loadChildren: () => ProductModule},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SaleRoutingModule {}