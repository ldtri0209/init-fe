import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SaleRoutingModule} from './routes';

import {SaleComponent} from './component';

@NgModule({
    imports: [
        CommonModule,
        SaleRoutingModule
    ],
    declarations: [
        SaleComponent
    ]
})
export class SaleModule {}