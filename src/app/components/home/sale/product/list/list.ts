import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {CurrencyPipe} from '@angular/common';
import {Util} from '../../../../../_libraries/util';
import {Define} from '../../../../../_libraries/define';
import {SaleService} from '../../../../../services/sale';
declare const $: any;
declare const moment: any;
declare const swal: any;

@Component({
    templateUrl: 'list.html',
    styles: ['.table > tfoot > tr > th { text-transform: none; }'],
    providers: [CurrencyPipe]
})

export class ListComponent implements OnInit {
    order_types: any = Define.order_types;
    order_sources: any = Define.order_sources;
    order_status: any = Define.order_status;
    order_payment_methods: any = Define.order_payment_methods;
    product_status: any = Define.product_status;
    categories: any = Define.categories;

    params: any = {};
    filter: any = {};
    table: any;
    columns: Array<Object> = [
        { title: '', data: null },
        { title: '', data: null, width: '30px' },
        { title: 'ID', data: 'id', width: '30px' },
        { title: 'Created At', data: 'order.created_at', render: (data:any, type:any, row:any, meta:any) => {
            return moment(data).format('DD-MM-YYYY HH:mm');
        } },
        { title: 'Delivery Date', data: 'delivery_date', render: (data:any, type:any, row:any, meta:any) => {
            return moment(data).format('DD-MM-YYYY');
        } },
        { title: 'Name', data: 'name' },
        { title: 'SKU', data: 'sku' },
        { title: 'Supplier SKU', data: 'supplier_sku' },
        { title: 'Quantity', data: 'quantity' },
        { title: 'Status', data: 'status', render: (data:any, type:any, row:any, meta:any) => {
            return Define.transformStatusProduct(data);
        } },
        { title: 'Bill Code', data: 'order.bill_code' },
        { title: 'Base Price', data: 'base_price', render: (data:any, type:any, row:any, meta:any) => {
            return this.currency.transform(data, 'VND', true);
        } },
        { title: 'Price', data: 'price', render: (data:any, type:any, row:any, meta:any) => {
            return this.currency.transform(data, 'VND', true);
        }, visible: false },
        { title: 'Category', data: 'supplier_cate_id', render: (data:any, type:any, row:any, meta:any) => {
            return Define.transformCategory(data);
        }},
        { title: 'Customer Name', data: 'order.address.name', visible: false },
        { title: 'Order Type', data: 'order.is_retail', render: (data:any, type:any, row:any, meta:any) => {
            return Define.transformTypeOrder(data);
        }, visible: false },
        { title: 'Source', data: 'order.cus_source_id', render: (data:any, type:any, row:any, meta:any) => {
            return Define.transformSourceOrder(data);
        }, visible: false },
        { title: 'Assigners', data: {_: 'order.assigns', filter: 'order.assigns.user.name'}, render: (data:any, type:any, row:any, meta:any) => {
            let value = '';

            data.map((v:any, i:any) => {
                if (v.user) {
                    value += '<span class="badge badge-pill badge-primary" style="margin-right: 2px;">' + v.user.name + '</span>';
                }
            });

            return value;
        }, visible: false },
        { title: 'Status Order', data: 'order.status', render: (data:any, type:any, row:any, meta:any) => {
            return Define.transformStatusOrder(data);
        }, visible: false },
        { title: 'Payment Method', data: 'order.payment_method', render: (data:any, type:any, row:any, meta:any) => {
            return Define.transformPaymentMethodOrder(data);
        }, visible: false },
        { title: 'Address', data: 'order.address.address', visible: false },
        { title: 'City', data: 'order.address.city', visible: false },
        { title: 'District', data: 'order.address.district', visible: false },
        { title: 'Phone', data: 'order.address.phone_number', visible: false },
        { title: 'Email', data: 'order.address.email', visible: false },
        { title: '', data: null, width: '30px' }
    ];
    input: any = {
        status: '',
        delivery_date: '',
        supplier_cate_id: ''
    };
    quickFilterValue: string = '';
    selectedProductIds:any = [];

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private currency: CurrencyPipe,
        private service: SaleService
    ) {
        route.queryParams.subscribe(params => this.params = params);
    }

    ngOnInit() {
        $('.select2').select2({minimumResultsForSearch: Infinity, tags: true});
        $.fn.dataTable.ext.errMode = 'none';
        this.table = $('#data-table').DataTable({
            dom: "<'row'<'col-sm-2'l><'col-sm-4 data-filter'><'col-sm-6'C>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            colReorder: true,
            stateSave: true,
            stateSaveCallback: function(settings:any, data:any) {
                localStorage.setItem('DataTables_' + settings.sInstance + '_sale_product', JSON.stringify(data));
            },
            stateLoadCallback: function(settings:any) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance + '_sale_product'));
            },
            stateLoadParams: (settings:any, data:any) => {
                data.search.search = '';
                data.columns.map((v:any,i:any) => {
                    v.search.search = '';
                });
            },
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            columns: this.columns,
            columnDefs: [
                {
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: ''
                },
                {
                    targets: 1,
                    responsivePriority: 1,
                    orderable: false,
                    searchable: false,
                    render: function (data:any, type:any, row:any, meta:any) {
                        return '<ul class="table-options"><li><input type="checkbox" class="form-control select-row"></li></ul>';
                    }
                },
                { targets: 2, responsivePriority: 2},
                {
                    targets: -1,
                    responsivePriority: 2,
                    orderable: false,
                    searchable: false,
                    render: function (data:any, type:any, row:any, meta:any) {
                        return '<ul class="table-options"><li><a class="edit" style="cursor: pointer"><i class="fa fa-pencil"></i></a></li></ul>';
                    }
                }
            ],
            order: [[2, 'desc']],
            serverSide: true,
            ajax: (data:any, callback:any, settings:any) => {
                $('#filter').detach().appendTo('.data-filter');
                let params = {};
                params['order'] = {
                    data: data.columns[data.order[0].column].data,
                    value: data.order[0].dir
                };
                params['limit'] = data.length;
                params['page'] = (data.start/data.length)+1;
                params['columns'] = {};
                params['custom'] = {};
                data.columns.map((value:any, index:any) => {
                    if (value.search.value) {
                        if (value.data.split('.').length > 1) value.data = value.data.replace('.', '-');
                        params['columns'][value.data] = {
                            value: value.search.value,
                            regex: value.search.regex
                        };
                    }
                });

                if (this.quickFilterValue === '3') {
                    params['custom']['has_purchase'] = {
                        value: true,
                        regex: false
                    };
                }

                this.filter = params;
                this.service.getProduct(params)
                    .subscribe(
                        (res: Response | any) => {
                            let callbackData = {
                                data: res.results.data,
                                draw: data.draw,
                                recordsFiltered: res.results.total,
                                recordsTotal: res.results.total
                            };
                            callback(callbackData);
                        }
                    );
            },
            rowCallback: (row:any, data:any, index:any) => {
                setTimeout(() => {
                    $(row).find('td:last-child .edit').on('click', () => {
                        this.router.navigate(['/sale/product/edit', data.id]);
                    });
                    $(row).find('td .select-row').on('change', (e: any) => {
                        let $this = $(e.target);
                        if ($this.is(':checked')) {
                            this.selectedProductIds.push(data.id);
                        } else {
                            this.selectedProductIds.splice(this.selectedProductIds.indexOf(data.id), 1);
                        }
                    });
                }, 1);
            },
            initComplete: (settings:any, json:any) => {
                $('input[type="daterange"]').daterangepicker({
                    autoApply: true,
                    autoUpdateInput: false,
                    showDropdowns: true,
                    linkedCalendars: false,
                    locale: {
                        format: 'DD-MM-YYYY',
                        separator: '|'
                    }
                }).on('apply.daterangepicker', function(event:any, picker:any) {
                    $(this).val(picker.startDate.format('DD-MM-YYYY') + '|' + picker.endDate.format('DD-MM-YYYY')).trigger('change');
                });

                if (this.params['quick_filter_value']) {
                    this.quickFilterValue = this.params['quick_filter_value'];
                    this.quickFilter(this.quickFilterValue);
                }
            },
            oLanguage: {
                oPaginate: {
                    sNext: '<i class="fa fa-angle-right"></i>',
                    sPrevious: '<i class="fa fa-angle-left"></i>'
                }
            },
            drawCallback: function () {
                $('.dataTables_paginate > .pagination').addClass('pagination-bordered');
            }
        }).columns().every(function() {
            let column = this;
            $('input', column.footer()).on('change', function() {
                if (column.search() !== this.value) {
                    let type = $(this).data('type');
                    let value: any = [];
                    this.value.split('|').map((v: any, i: any) => {
                        if (type === 'date') {
                            if (i === 0) v = moment.utc(v, 'DD-MM-YYYY').hour(0).minute(0).second(0).toISOString();
                            if (i === 1) v = moment.utc(v, 'DD-MM-YYYY').hour(23).minute(59).second(59).toISOString();
                        }
                        value.push(v);
                    });
                    column.search(value.join('|'), type === 'regex').draw();
                }
            });
            $('.select2', this.footer()).select2().on('select2:select select2:unselect', function (e:any) {
                let data = $(this).select2('data');
                let temp: any = [];
                data.map((value:any, index:any) => {
                    let split = value.id.split(': ');
                    if (split.length > 1)temp.push(split[1]);
                    else temp.push(value.id);
                });
                column.search(temp).draw();
            });
        });
    }

    quickFilter(value:any) {
        switch (value) {
            case '1': {
                let ele = $('#status').select2();
                let value:any = [];
                this.product_status.map((v:any, i:any) => {
                    if (v.value != 10 && v.value != 11) {
                        value.push(i+': '+v.value)
                    }
                });
                ele.val(value);
                ele.trigger('change');
            } break;
            case '2': {
                this.input.delivery_date = moment().format('DD-MM-YYYY')+'|'+moment().add(2, 'days').format('DD-MM-YYYY');
            } break;
            case '3': {
                let eleStatus = $('#status').select2();
                let eleCategory = $('#category').select2();
                let categories:any = [];

                this.categories.map((v:any, i:any) => {
                    if ([2115, 2106, 2123, 2101, 2102, 2103, 2104, 2105, 2114].indexOf(parseInt(v.cate_id)) !== -1) {
                        categories.push(i+': '+v.cate_id)
                    }
                });

                eleStatus.val(['0: 1']);
                eleCategory.val(categories);

                eleStatus.trigger('change');
                eleCategory.trigger('change');
            } break;
            default: {
                let ele = $('.select2').select2();
                ele.val([]);
                ele.trigger('change');
                this.input.delivery_date = '';
            } break;
        }
        setTimeout(() => {
            this.table.columns().every(function() {
                let input = $('input', this.footer());
                if (typeof input.val() !== 'undefined' && this.search() !== input.val()) {
                    let type = input.data('type');
                    let value: any = [];
                    input.val().split('|').map((v: any, i: any) => {
                        if (type === 'date') {
                            if (i === 0) v = moment.utc(v, 'DD-MM-YYYY').hour(0).minute(0).second(0).toISOString();
                            if (i === 1) v = moment.utc(v, 'DD-MM-YYYY').hour(23).minute(59).second(59).toISOString();
                        }
                        value.push(v);
                    });
                    this.search(value.join('|'), type === 'regex');
                }
                let select2 = $('.select2', this.footer());
                if (typeof select2.val() !== 'undefined' && this.search() !== select2.val()) {
                    let temp: any = [];
                    select2.val().map((value:any, index:any) => {
                        temp.push(value.split(': ')[1]);
                    });
                    this.search(temp);
                }
            }).draw();
        }, 1);
    }

    export() {
        delete this.filter.limit;
        this.service.getProduct(this.filter)
            .subscribe(
                (res: Response | any) => {
                    Util.exportAsExcelFile(res.results.data, this.columns, 'sale_product');
                }
            );
    }

    generatePurchases() {
        if (this.selectedProductIds.length) {
            this.service.generatePurchases({ product_ids: this.selectedProductIds })
                .subscribe(
                    (res: Response | any) => {
                        this.selectedProductIds = [];
                        swal.setDefaults({
                            onClose: () => {
                                this.table.ajax.reload();
                            }
                        });
                        swal('Success', 'Created purchases successfully', 'success');
                    }
                );
        }
    }
}