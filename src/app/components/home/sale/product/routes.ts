import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProductComponent} from './component';
import {ListComponent} from './list/list';
import {AddComponent} from './action/add';
import {EditComponent} from './action/edit';

const routes: Routes = [
    {
        path: '',
        component: ProductComponent,
        data: {name: 'Product'},
        children: [
            {path: '', component: ListComponent, data: {name: 'List'}},
            {path: 'list', component: ListComponent, data: {name: 'List'}},
            {path: 'add', component: AddComponent, data: {name: 'Create'}},
            {path: 'edit/:id', component: EditComponent, data: {name: 'Edit'}}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProductRoutingModule {}