import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {ProductRoutingModule} from './routes';

import {ProductComponent} from './component';
import {ListComponent} from './list/list';
import {AddComponent} from './action/add';
import {EditComponent} from './action/edit';

import {SaleService} from '../../../../services/sale';

@NgModule({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        ProductRoutingModule
    ],
    declarations: [
        ProductComponent,
        ListComponent,
        AddComponent,
        EditComponent
    ],
    providers: [SaleService]
})
export class ProductModule {}