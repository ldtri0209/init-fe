import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CurrencyPipe} from '@angular/common';
import {DialogService} from 'ng2-bootstrap-modal';
import {Define} from '../../../../../_libraries/define';
import {SaleService} from '../../../../../services/sale';
import {OrderModal} from '../modal/order';
import {AddressModal} from '../modal/address';
declare const $: any;
declare const moment: any;

@Component({
    templateUrl: 'edit.html',
    styleUrls: ['edit.scss'],
    providers: [CurrencyPipe]
})

export class EditComponent implements OnInit {
    submitted: boolean = false;
    params: any = {};
    data: any = {
        address: {},
        assigns: [],
        comments: [],
        activities: [],
        products: []
    };
    optionsEditor: Object = {
        toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'fontFamily', 'fontSize', 'insertLink', 'undo', 'redo']
    };
    comment: any = {
        order_id: null,
        content: ''
    };
    users: any = [];

    columns: Array<Object> = [
        { title: '', data: null },
        { title: 'ID', data: 'id', width: '30px' },
        { title: 'Name', data: 'name' },
        { title: 'SKU', data: 'sku' },
        { title: 'Supplier SKU', data: 'supplier_sku' },
        { title: 'Quantity', data: 'quantity' },
        { title: 'Status', data: 'status', render: (data:any, type:any, row:any, meta:any) => {
            return Define.transformStatusProduct(data);
        } },
        { title: 'Base Price', data: 'base_price', render: (data:any, type:any, row:any, meta:any) => {
            return this.currency.transform(data, 'VND', true);
        } },
        { title: 'Price', data: 'price', render: (data:any, type:any, row:any, meta:any) => {
            return this.currency.transform(data, 'VND', true);
        } },
        { title: 'Category', data: 'supplier_cate_id', render: (data:any, type:any, row:any, meta:any) => {
            return Define.transformCategory(data);
        } }
    ];

    constructor(
        private route: ActivatedRoute,
        private currency: CurrencyPipe,
        private dialog: DialogService,
        private service: SaleService
    ) {
        route.params.subscribe(params => {
            this.params = params;
            this.comment.order_id = parseInt(this.params['id']);
        });
        this.getUser();
        this.loadData();
    }

    getUser() {
        this.service.getUser()
            .subscribe(
                (res: Response | any) => {
                    this.users = res.results.data;
                }
            )
    }

    ngOnInit() {
        let select2 = $('#user').select2();
        $('.popover-button').popover({
            placement: 'bottom',
            html: true,
            content: $("#popover-content")
        }).on('show.bs.popover', () => {
            $("#popover-content").show();
            let assigns:any = [];
            this.data.assigns.map((value:any, index:any) => {
                assigns.push(value.user_id);
            });
            select2.val(assigns).change();
        });
    }

    loadData() {
        this.service.getOrderById(this.params['id'])
            .subscribe(
                (res: Response | any) => {
                    res.results.discount = (!res.results.promotion_type) ? res.results.promotion_value * res.results.total_base_price / 100 : res.results.promotion_value;
                    this.data = res.results;

                    $.fn.dataTable.ext.errMode = 'none';
                    $('#products').DataTable({
                        dom: "<'row'<'col-sm-6'><'col-sm-6'C>><'row'<'col-sm-12'tr>>",
                        colReorder: true,
                        stateSave: true,
                        stateSaveCallback: function (settings: any, data: any) {
                            localStorage.setItem('DataTables_' + settings.sInstance + '_products', JSON.stringify(data));
                        },
                        stateLoadCallback: function (settings: any) {
                            return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance + '_products'));
                        },
                        stateLoadParams: (settings: any, data: any) => {
                            data.search.search = '';
                            data.columns.map((v: any, i: any) => {
                                v.search.search = '';
                            });
                        },
                        responsive: {
                            details: {
                                type: 'column',
                                target: 'tr'
                            }
                        },
                        columns: this.columns,
                        columnDefs: [
                            {
                                targets: 0,
                                className: 'control',
                                orderable: false,
                                searchable: false,
                                defaultContent: ''
                            },
                            {targets: 1, responsivePriority: 1}
                        ],
                        order: [[1, 'desc']],
                        data: this.data.products
                    });
                }
            );
    }

    showModalOrder(data: any) {
        return this.dialog.addDialog(OrderModal, {
            title: data.bill_code,
            data: data
        }, {
            closeByClickingOutside: true
        }).subscribe(
            (res:any) => {
                if (res) {
                    Object.assign(this.data, res);
                }
            }
        );
    }

    showModalAddress(data: any) {
        return this.dialog.addDialog(AddressModal, {
            title: data.bill_code,
            data: data
        }, {
            closeByClickingOutside: true
        }).subscribe(
            (res:any) => {
                if (res) {
                    Object.assign(this.data.address, res);
                }
            }
        );
    }

    assign() {
        this.service.assignByOrderId(this.params['id'], $('#user').val())
            .subscribe(
                (res: Response | any) => {
                    this.data.assigns = res.results;
                    $('.popover-button').popover('hide');
                }
            );
    }

    closePopover() {
        $('.popover-button').popover('hide');
    }

    addComment() {
        if (this.comment.content !== '') {
            this.service.commentByOrderId(this.comment)
                .subscribe(
                    (res: Response | any) => {
                        this.data.comments.unshift(res.results);
                        this.comment.content = '';
                    }
                );
        }
    }
}