import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FroalaEditorModule, FroalaViewModule} from 'angular-froala-wysiwyg';

import {SharedModule} from '../../../shared/shared.module';

import {OrderRoutingModule} from './routes';

import {OrderComponent} from './component';
import {ListComponent} from './list/list';
import {AddComponent} from './action/add';
import {EditComponent} from './action/edit';

import {OrderModal} from './modal/order';
import {AddressModal} from './modal/address';

import {SaleService} from '../../../../services/sale';

@NgModule({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
        SharedModule,
        OrderRoutingModule
    ],
    declarations: [
        OrderComponent,
        ListComponent,
        AddComponent,
        EditComponent,
        OrderModal,
        AddressModal
    ],
    entryComponents: [
        OrderModal,
        AddressModal
    ],
    providers: [SaleService]
})
export class OrderModule {}