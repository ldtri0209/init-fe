import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DialogComponent, DialogService} from 'ng2-bootstrap-modal';
import {Define} from '../../../../../_libraries/define';
import {SaleService} from '../../../../../services/sale';

export interface Model {
    title: string;
    data: any;
}
@Component({
    templateUrl: 'address.html'
})
export class AddressModal extends DialogComponent<Model, any> implements Model, OnInit {
    cities: any = Define.cities;
    districts: any = Define.districts;
    districts_tmp: any;

    form: FormGroup;
    submitted: boolean = false;

    title: string;
    data: any;

    constructor(
        public dialog: DialogService,
        private service: SaleService
    ) {
        super(dialog);
        this.districts_tmp = this.districts;
    }

    ngOnInit() {
        document.getElementsByTagName('body')[0].classList.add('modal-open');
        this.buildForm();
    }

    ngOnDestroy(){
        document.getElementsByTagName('body')[0].classList.remove('modal-open');
    }

    buildForm() {
        this.form = new FormGroup({
            name: new FormControl('', [Validators.required]),
            phone_number: new FormControl('', [Validators.required]),
            email: new FormControl(''),
            address: new FormControl('', [Validators.required]),
            ward: new FormControl('', [Validators.required]),
            district: new FormControl('', [Validators.required]),
            city: new FormControl('', [Validators.required]),
        });

        this.loadData();
    }

    loadData() {
        this.form.patchValue(this.data.address);
        if (this.form.controls['city'].value !== '') {
            this.getDistrictByCity(this.form.controls['city'].value);
        }
    }

    onSubmit() {
        this.submitted = true;
        if (this.form.valid) {
            this.service.updateAddressByOrderId(this.data.id, this.form.value)
                .subscribe(
                    (res: Response | any) => {
                        this.result = res.results;
                        this.close();
                    }
                );
        }
    }

    getDistrictByCity(cityName: any) {
        this.cities.map((value:any, index:any) => {
            if (value.name == cityName) {
                let districts:any = [];
                this.districts.map((val: any, ind: any) => {
                    if (val.code_city == value.code) {
                        districts.push(val);
                    }
                });
                this.districts_tmp = districts;
                return false;
            }
        });
    }
}