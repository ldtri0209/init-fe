import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DialogComponent, DialogService} from 'ng2-bootstrap-modal';
import {Define} from '../../../../../_libraries/define';
import {SaleService} from '../../../../../services/sale';

export interface Model {
    title: string;
    data: any;
}
@Component({
    templateUrl: 'order.html'
})
export class OrderModal extends DialogComponent<Model, any> implements Model, OnInit {
    order_status: any = Define.order_status;
    order_types: any = Define.order_types;
    order_sources: any = Define.order_sources;
    order_payment_methods: any = Define.order_payment_methods;

    form: FormGroup;
    submitted: boolean = false;

    title: string;
    data: any;

    constructor(
        public dialog: DialogService,
        private service: SaleService
    ) {
        super(dialog);
    }

    ngOnInit() {
        document.getElementsByTagName('body')[0].classList.add('modal-open');
        this.buildForm();
    }

    ngOnDestroy(){
        document.getElementsByTagName('body')[0].classList.remove('modal-open');
    }

    buildForm() {
        this.form = new FormGroup({
            status: new FormControl('', [Validators.required]),
            is_retail: new FormControl('', [Validators.required]),
            cus_source_id: new FormControl('', [Validators.required]),
            payment_method: new FormControl('', [Validators.required]),
            customer_paid_amount: new FormControl('', [Validators.required]),
        });

        this.loadData();
    }

    loadData() {
        this.form.patchValue(this.data);
    }

    onSubmit() {
        this.submitted = true;
        if (this.form.valid) {
            this.service.updateOrderById(this.data.id, this.form.value)
                .subscribe(
                    (res: Response | any) => {
                        this.result = res.results;
                        this.close();
                    }
                );
        }
    }
}