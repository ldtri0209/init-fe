import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Util} from '../../../../../_libraries/util';
import {Define} from '../../../../../_libraries/define';
import {SaleService} from '../../../../../services/sale';
declare const $: any;
declare const moment: any;
declare const swal: any;

@Component({
    templateUrl: 'list.html',
    styles: ['.table > tfoot > tr > th { text-transform: none; }']
})

export class ListComponent implements OnInit {
    order_types: any = Define.order_types;
    order_sources: any = Define.order_sources;
    order_status: any = Define.order_status;
    order_payment_methods: any = Define.order_payment_methods;

    filter: any = {};
    table: any;
    columns: Array<Object> = [
        { title: '', data: null },
        { title: 'ID', data: 'id', width: '30px' },
        { title: 'Created At', data: 'created_at', render: (data:any, type:any, row:any, meta:any) => {
            return moment(data).format('DD-MM-YYYY HH:mm');
        } },
        { title: 'Delivery Date', data: 'delivery_date', render: (data:any, type:any, row:any, meta:any) => {
            return moment(data).format('DD-MM-YYYY');
        } },
        { title: 'Bill Code', data: 'bill_code' },
        { title: 'Customer Name', data: 'address.name' },
        { title: 'Order Type', data: 'is_retail', render: (data:any, type:any, row:any, meta:any) => {
            return Define.transformTypeOrder(data);
        } },
        { title: 'Source', data: 'cus_source_id', render: (data:any, type:any, row:any, meta:any) => {
            return Define.transformSourceOrder(data);
        } },
        { title: 'Assigners', data: {_: 'assigns', filter: 'assigns.user.name'}, render: (data:any, type:any, row:any, meta:any) => {
            let value = '';

            data.map((v:any, i:any) => {
                if (v.user) {
                    value += '<span class="badge badge-pill badge-primary" style="margin-right: 2px;">' + v.user.name + '</span>';
                }
            });

            return value;
        } },
        { title: 'Status', data: 'status', render: (data:any, type:any, row:any, meta:any) => {
            return Define.transformStatusOrder(data);
        } },
        { title: 'Payment Method', data: 'payment_method', render: (data:any, type:any, row:any, meta:any) => {
            return Define.transformPaymentMethodOrder(data);
        } },
        { title: 'Address', data: 'address.address', visible: false },
        { title: 'City', data: 'address.city', visible: false },
        { title: 'District', data: 'address.district', visible: false },
        { title: 'Phone', data: 'address.phone_number', visible: false },
        { title: 'Email', data: 'address.email', visible: false },
        { title: '', data: null, width: '30px' }
    ];
    selectedOrderIds:any = [];

    constructor(
        private router: Router,
        private service: SaleService
    ) {}

    ngOnInit() {
        $('.select2').select2({minimumResultsForSearch: Infinity, tags: true});
        $.fn.dataTable.ext.errMode = 'none';
        this.table = $('#data-table').DataTable({
            dom: "<'row'<'col-sm-6'l><'col-sm-6'C>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            colReorder: true,
            stateSave: true,
            stateSaveCallback: function(settings:any, data:any) {
                localStorage.setItem('DataTables_' + settings.sInstance + '_sale_order', JSON.stringify(data));
            },
            stateLoadCallback: function(settings:any) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance + '_sale_order'));
            },
            stateLoadParams: (settings:any, data:any) => {
                data.search.search = '';
                data.columns.map((v:any,i:any) => {
                    v.search.search = '';
                });
            },
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            columns: this.columns,
            columnDefs: [
                {
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: ''
                },
                { targets: 1, responsivePriority: 1},
                {
                    targets: -1,
                    responsivePriority: 2,
                    orderable: false,
                    searchable: false,
                    render: function (data:any, type:any, row:any, meta:any) {
                        return '<ul class="table-options"><li><a class="edit" style="cursor: pointer"><i class="fa fa-pencil"></i></a></li></ul>';
                    }
                }
            ],
            order: [[1, 'desc']],
            serverSide: true,
            ajax: (data:any, callback:any, settings:any) => {
                let params = {};
                params['limit'] = data.length;
                params['page'] = (data.start/data.length)+1;
                params['order'] = {
                    data: (typeof data.columns[data.order[0].column].data === 'object') ? data.columns[data.order[0].column].data.filter : data.columns[data.order[0].column].data,
                    value: data.order[0].dir
                };
                params['columns'] = {};
                data.columns.map((value:any, index:any) => {
                    if (value.search.value) {
                        if (typeof value.data === 'object') value.data = value.data.filter;
                        if (value.data.split('.').length > 1) value.data = value.data.split('.').join('-');
                        params['columns'][value.data] = {
                            value: value.search.value,
                            regex: value.search.regex
                        };
                    }
                });
                this.filter = params;
                this.service.getOrder(params)
                    .subscribe(
                        (res: Response | any) => {
                            let callbackData = {
                                data: res.results.data,
                                draw: data.draw,
                                recordsFiltered: res.results.total,
                                recordsTotal: res.results.total
                            };
                            callback(callbackData);
                        }
                    );
            },
            rowCallback: (row:any, data:any, index:any) => {
                setTimeout(() => {
                    $(row).find('td:last-child .edit').on('click', () => {
                        this.router.navigate(['/sale/order/edit', data.id]);
                    });
                }, 1);
            },
            initComplete: (settings:any, json:any) => {
                $('input[type="daterange"]').daterangepicker({
                    autoApply: true,
                    autoUpdateInput: false,
                    showDropdowns: true,
                    linkedCalendars: false,
                    locale: {
                        format: 'DD-MM-YYYY',
                        separator: '|'
                    }
                }).on('apply.daterangepicker', function(event:any, picker:any) {
                    $(this).val(picker.startDate.format('DD-MM-YYYY') + '|' + picker.endDate.format('DD-MM-YYYY')).trigger('change');
                });
            },
            oLanguage: {
                oPaginate: {
                    sNext: '<i class="fa fa-angle-right"></i>',
                    sPrevious: '<i class="fa fa-angle-left"></i>'
                }
            },
            drawCallback: function () {
                $('.dataTables_paginate > .pagination').addClass('pagination-bordered');
            }
        }).columns().every(function() {
            let column = this;
            $('input', column.footer()).on('change', function() {
                if (column.search() !== this.value) {
                    let type = $(this).data('type');
                    let value: any = [];
                    this.value.split('|').map((v: any, i: any) => {
                        if (type === 'date') {
                            if (i === 0) v = moment.utc(v, 'DD-MM-YYYY').hour(0).minute(0).second(0).toISOString();
                            if (i === 1) v = moment.utc(v, 'DD-MM-YYYY').hour(23).minute(59).second(59).toISOString();
                        }
                        value.push(v);
                    });
                    column.search(value.join('|'), type === 'regex').draw();
                }
            });
            $('.select2', this.footer()).select2().on('select2:select select2:unselect', function (e:any) {
                let data = $(this).select2('data');
                let temp: any = [];
                data.map((value:any, index:any) => {
                    temp.push(value.id);
                });
                column.search(temp).draw();
            });
        });
    }

    export() {
        delete this.filter.limit;
        this.service.getOrder(this.filter)
            .subscribe(
                (res: Response | any) => {
                    Util.exportAsExcelFile(res.results.data, this.columns, 'sale_order');
                }
            );
    }
}