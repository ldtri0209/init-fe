import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {AUTH_PROVIDERS} from 'angular2-jwt';

import {AppRoutingModule} from './app.routes';

import {AppComponent} from './app.component';
import {LoginComponent} from './components/front/login/login';
import {Page404Component} from './components/front/pages/index';

import {AuthGuard} from './_guards/auth.guard';
import {HttpClient} from './_libraries/http.client';
import {Util} from './_libraries/util';
import {Define} from './_libraries/define';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule, ReactiveFormsModule,
        HttpModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        Page404Component
    ],
    providers: [AUTH_PROVIDERS, AuthGuard, HttpClient, Util, Define],
    bootstrap: [AppComponent]
})
export class AppModule {}