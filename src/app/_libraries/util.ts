import {Injectable} from '@angular/core';
declare const moment: any;
declare const FileSaver: any;
import * as XLSX from 'xlsx';
import {Define} from './define';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class Util {
    static dataURItoBlob(dataURI: string) {
        let byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0) {
            byteString = atob(dataURI.split(',')[1]);
        }
        let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        let ia = new Uint8Array(byteString.length);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ia], {type:mimeString});
    }

    static exportAsExcelFile(json: any[], columns: any[], excelFileName: string): void {
        let data:any = [];
        json.map((value:any, index:any) => {
            let temp:any = [];
            columns.map((val:any, ind:any) => {
                if (val.data) {
                    let filter = '';
                    let field = val.data;
                    if (typeof field === 'object') {
                        filter = val.data.filter;
                        field = val.data._;
                    }
                    let dateFlag = false;
                    if (field.indexOf('_date') >= 0) dateFlag = true;
                    let join = field.split('.');
                    if (join.length > 1) {
                        temp[field] = this.serializeData(field, excelFileName, this.serializeObject(value, join, field, filter.split('.').slice(), dateFlag, excelFileName), dateFlag);
                    } else {
                        if (Array.isArray(value[field])) {
                            let array:any = [];
                            value[field].map((v:any, i:any) => {
                                array.push(this.serializeData(field, excelFileName, this.serializeArray(v, filter.split('.').slice()), dateFlag));
                            });
                            temp[field] = array.join('|');
                        } else {
                            temp[field] = this.serializeData(field, excelFileName, value[field], dateFlag);
                        }
                    }
                }
            });
            data.push(temp);
        });
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

    static serializeObject(target:any, array:any, column?:any, filterArr?:any, dateFlag?: Boolean, excelFileName?: String):any {
        for (let i = 1; i <= array.length; i++) {
            if (typeof target[array[i - 1]] === 'undefined' || !target[array[i - 1]]) return '';
            else if (typeof target[array[i - 1]][array[i]] === 'undefined' || !target[array[i - 1]][array[i]]) return '';

            if (Array.isArray(target[array[i - 1]][array[i]])) {
                let array2:any = [];
                let newFilterArr = filterArr.slice();
                newFilterArr.splice(i - 1, 1);
                let tempNewFilterArr = newFilterArr.slice();
                target[array[i - 1]][array[i]].map((v:any, i:any) => {
                    array2.push(this.serializeArray(v, tempNewFilterArr.slice()));
                });
                return array2.join('|');
            }
            if (typeof target[array[i - 1]][array[i]] === 'object') {
                let newArray = array.slice();
                newArray.splice(i - 1, 1);
                return this.serializeObject(target[array[i - 1]], newArray);
            }
            return target[array[i - 1]][array[i]];
        }
    }

    static serializeArray(target:any, array:any):any {
        array.splice(0, 1);
        for (let i = 0; i < array.length; i++) {
            if (typeof target[array[i]] === 'object') {
                return this.serializeArray(target[array[i]], array);
            }
            return target[array[i]];
        }
    }

    static serializeData(field:String, excelFileName:String, target:String, dateFlag:Boolean) {
        if (moment(target, moment.ISO_8601).isValid()) {
            if (dateFlag) return moment(target).format('DD-MM-YYYY');
            return moment(target).format('DD-MM-YYYY HH:mm');
        }
        if (field.indexOf('is_retail') >= 0) target = Define.transformTypeOrder(target);
        if (field.indexOf('cus_source_id') >= 0) target = Define.transformSourceOrder(target);
        if (field.indexOf('payment_method') >= 0) target = Define.transformPaymentMethodOrder(target);
        if (field.indexOf('supplier_cate_id') >= 0) target = Define.transformCategory(target);
        if (field.indexOf('status') >= 0) {
            if (excelFileName.indexOf('order') >= 0 || field.indexOf('order') >= 0) target = Define.transformStatusOrder(target);
            else if (excelFileName.indexOf('product') >= 0 || field.indexOf('product') >= 0) target = Define.transformStatusProduct(target);
            else if (excelFileName.indexOf('purchase') >= 0 || field.indexOf('purchase') >= 0) target = Define.transformStatusPurchase(target);
        }
        return target;
    }

    static saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    }
}