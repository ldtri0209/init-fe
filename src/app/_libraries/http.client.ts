import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Headers, Response, RequestOptionsArgs, RequestOptions} from '@angular/http';
import {AuthHttp} from 'angular2-jwt';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs';
declare const $: any;
declare const swal: any;

@Injectable()
export class HttpClient {
    apiUrl: string;
    options: any = {};

    constructor(
        private http: AuthHttp,
        private router: Router
    ) {
        this.apiUrl = process.env.apiUrl;
        if (localStorage.getItem('id_token')) {
            let headers = new Headers();
            headers.append('Accept-Language', JSON.parse(localStorage.getItem('profile')).lang);
            this.options = new RequestOptions({
                headers: headers
            });
        }
    }

    private extractData = (res: Response | any) => {
        $('.loader').removeClass('is-active');
        return res.json();
    };

    private handleError = (err: Response | any) => {
        $('.loader').removeClass('is-active');
        if (typeof err.status !== 'undefined' && err.status === 401) {
            return this.router.navigate(['/login'], {queryParams: {returnUrl: this.router.url}});
        }
        let message = (typeof err.json().message === 'object') ? JSON.stringify(err.json().message) : err.json().message;

        if (!message && err.json().errors && err.json().errors.message) {
            message = err.json().errors.message;
        }

        swal('Error code: ' + err.status, message, 'error');
        return Observable.throw(err.json());
    };

    get(url: string, params?: Object, options?: RequestOptionsArgs, loading: Boolean = true) {
        if (loading) $('.loader').addClass('is-active');
        let fullUrl = url;
        if (typeof params !== 'undefined') fullUrl = url + '?' + $.param(params);
        return this.http.get(this.apiUrl + fullUrl, Object.assign(this.options, options))
            .map(this.extractData)
            .catch(this.handleError);
    }

    post(url: string, data?: any, options?: RequestOptionsArgs, loading: Boolean = true) {
        if (loading) $('.loader').addClass('is-active');
        return this.http.post(this.apiUrl + url, data, Object.assign(this.options, options))
            .map(this.extractData)
            .catch(this.handleError);
    }

    put(url: string, data?: any, options?: RequestOptionsArgs, loading: Boolean = true) {
        if (loading) $('.loader').addClass('is-active');
        return this.http.put(this.apiUrl + url, data, Object.assign(this.options, options))
            .map(this.extractData)
            .catch(this.handleError);
    }

    delete(url: string, options?: RequestOptionsArgs, loading: Boolean = true) {
        if (loading) $('.loader').addClass('is-active');
        return this.http.delete(this.apiUrl + url, Object.assign(this.options, options))
            .map(this.extractData)
            .catch(this.handleError);
    }

    patch(url: string, data?: any, options?: RequestOptionsArgs, loading: Boolean = true) {
        if (loading) $('.loader').addClass('is-active');
        return this.http.patch(this.apiUrl + url, data, Object.assign(this.options, options))
            .map(this.extractData)
            .catch(this.handleError);
    }
}