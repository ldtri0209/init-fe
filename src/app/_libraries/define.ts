import {Injectable} from '@angular/core';

@Injectable()
export class Define {
    static status: Array<Object> = [
        {value: 0, name: 'In-Active'},
        {value: 1, name: 'Active'},
    ];
    static transformStatus(target: any) {
        let name = '';
        this.status.map((v:any,i:any) => {
            if (v['value'] == target) {
                name = v['name'];
            }
        });
        return name;
    }

    static stock_log_types: Array<Object> = [
        {value: 1, name: 'Add stock'},
        {value: 2, name: 'Minus stock'},
        {value: 3, name: 'Add reserved'},
        {value: 4, name: 'Minus reserved'},
        {value: 5, name: 'Set stock number'},
        {value: 6, name: 'Sync stock'},
    ];
    static transformStockLogType(target: any) {
        let name = '';
        this.stock_log_types.map((v:any,i:any) => {
            if (v['value'] == target) {
                name = v['name'];
            }
        });
        return name;
    }

    static categories: Array<Object> = localStorage.getItem('settings.categories')?JSON.parse(localStorage.getItem('settings.categories')):[];
    static transformCategory(target: any) {
        let name = '';
        this.categories.map((v:any,i:any) => {
            if (v['cate_id'] == target) {
                name = v['name'];
            }
        });
        return name;
    }

    static order_status: Array<Object> = localStorage.getItem('settings.order_status')?JSON.parse(localStorage.getItem('settings.order_status')):[];
    static transformStatusOrder(target: any) {
        let name = '';
        this.order_status.map((v:any,i:any) => {
            if (v['value'] == target) {
                name = v['name'];
            }
        });
        return name;
    }

    static order_sources: Array<Object> = localStorage.getItem('settings.order_sources')?JSON.parse(localStorage.getItem('settings.order_sources')):[];
    static transformSourceOrder(target: any) {
        let name = '';
        this.order_sources.map((v:any,i:any) => {
            if (v['value'] == target) {
                name = v['name'];
            }
        });
        return name;
    }

    static order_types: Array<Object> = localStorage.getItem('settings.order_types')?JSON.parse(localStorage.getItem('settings.order_types')):[];
    static transformTypeOrder(target: any) {
        let name = '';
        this.order_types.map((v:any,i:any) => {
            if (v['value'] == target) {
                name = v['name'];
            }
        });
        return name;
    }

    static order_payment_methods: Array<Object> = localStorage.getItem('settings.order_payment_methods')?JSON.parse(localStorage.getItem('settings.order_payment_methods')):[];
    static transformPaymentMethodOrder(target: any) {
        let name = '';
        this.order_payment_methods.map((v:any,i:any) => {
            if (v['value'] == target) {
                name = v['name'];
            }
        });
        return name;
    }

    static product_status: Array<Object> = localStorage.getItem('settings.product_status')?JSON.parse(localStorage.getItem('settings.product_status')):[];
    static transformStatusProduct(target: any) {
        let name = '';
        this.product_status.map((v:any,i:any) => {
            if (v['value'] == target) {
                name = v['name'];
            }
        });
        return name;
    }

    static purchase_status: Array<Object> = localStorage.getItem('settings.purchase_status')?JSON.parse(localStorage.getItem('settings.purchase_status')):[];
    static transformStatusPurchase(target: any) {
        let name = '';
        this.purchase_status.map((v:any,i:any) => {
            if (v['value'] == target) {
                name = v['name'];
            }
        });
        return name;
    }

    static cities: Array<Object> = localStorage.getItem('settings.cities')?JSON.parse(localStorage.getItem('settings.cities')):[];
    static transformCity(target: any) {
        let name = '';
        this.cities.map((v:any,i:any) => {
            if (v['id'] == target) {
                name = v['name'];
            }
        });
        return name;
    }

    static districts: Array<Object> = localStorage.getItem('settings.districts')?JSON.parse(localStorage.getItem('settings.districts')):[];
    static transformDistrict(target: any) {
        let name = '';
        this.districts.map((v:any,i:any) => {
            if (v['id'] == target) {
                name = v['name'];
            }
        });
        return name;
    }

    static suppliers: Array<Object> = localStorage.getItem('settings.suppliers')?JSON.parse(localStorage.getItem('settings.suppliers')):[];
    static transformSupplier(target: any) {
        let name = '';
        this.suppliers.map((v:any,i:any) => {
            if (v['id'] == target) {
                name = v['name'];
            }
        });
        return name;
    }

    static cate_attributes: Array<Object> = localStorage.getItem('settings.cate_attributes')?JSON.parse(localStorage.getItem('settings.cate_attributes')):[];
    static transformCateAttribute(cate: any, code: any) {
        let result = {};
        this.cate_attributes.map((v:any,i:any) => {
            if (v['supplier_cate_id'] == cate && v['attribute_code'] == code) {
                result = {
                    is_visible: v['is_visible'],
                    is_editable: v['is_editable']
                };
            }
        });
        return result;
    }
}