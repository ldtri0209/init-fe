import {Pipe, PipeTransform} from '@angular/core';
import {Define} from '../_libraries/define';

@Pipe({name: 'status_order'})
export class StatusOrderPipe implements PipeTransform {
    transform(value: any, innerHtml: boolean = false):any {
        if (innerHtml) {
            let badge = '';
            switch (parseInt(value)) {
                case 1: badge = 'default'; break;
                case 2: badge = 'primary'; break;
                case 4: badge = 'info'; break;
                case 5: badge = 'success'; break;
                case 6: badge = 'danger'; break;
            }
            return '<span class="badge badge-' + badge + '">' + Define.transformStatusOrder(value) + '</span>';
        }
        else return Define.transformStatusOrder(value);
    }
}