import {Pipe, PipeTransform} from '@angular/core';
import {Define} from '../_libraries/define';

@Pipe({name: 'payment_method_order'})
export class PaymentMethodOrderPipe implements PipeTransform {
    transform(value: any, ...args: any[]):any {
        return Define.transformPaymentMethodOrder(value);
    }
}