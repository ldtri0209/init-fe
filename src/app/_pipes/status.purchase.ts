import {Pipe, PipeTransform} from '@angular/core';
import {Define} from '../_libraries/define';

@Pipe({name: 'status_purchase'})
export class StatusPurchasePipe implements PipeTransform {
    transform(value: any, innerHtml: boolean = false):any {
        if (innerHtml) {
            let badge = '';
            switch (value) {
                case 'NW': badge = 'default'; break;
                case 'SM': badge = 'primary'; break;
                case 'CO': badge = 'info'; break;
                case 'DO': badge = 'success'; break;
            }
            return '<span class="badge badge-' + badge + '">' + Define.transformStatusPurchase(value) + '</span>';
        }
        else return Define.transformStatusPurchase(value);
    }
}