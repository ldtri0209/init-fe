import {Pipe, PipeTransform} from '@angular/core';
import {Define} from '../_libraries/define';

@Pipe({name: 'type_order'})
export class TypeOrderPipe implements PipeTransform {
    transform(value: any, ...args: any[]):any {
        return Define.transformTypeOrder(value);
    }
}