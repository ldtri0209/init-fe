import {Pipe, PipeTransform} from '@angular/core';
import {Define} from '../_libraries/define';

@Pipe({name: 'source_order'})
export class SourceOrderPipe implements PipeTransform {
    transform(value: any, ...args: any[]):any {
        return Define.transformSourceOrder(value);
    }
}