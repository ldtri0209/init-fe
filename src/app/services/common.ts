import {Injectable} from '@angular/core';
import {AuthHttp} from 'angular2-jwt';
import {HttpClient} from '../_libraries/http.client';
declare const $: any;

@Injectable()
export class CommonService {
    constructor(
        private _http: AuthHttp,
        public http: HttpClient
    ) {}

    getCategories() {
        return this._http.get(process.env.mgtUrl + '/product/listSupplierCate').map((res: Response | any) => res.json());
    }

    getProducts(params: Object) {
        return this._http.post(process.env.mgtUrl + '/product/search', params).map((res: Response | any) => res.json());
    }
}