import {Injectable} from '@angular/core';
import {HttpClient} from '../_libraries/http.client';
import {RequestOptionsArgs} from '@angular/http';

@Injectable()
export class SaleService {
    constructor(
        public http: HttpClient
    ) {}

    getOrder(params?: Object, options?: RequestOptionsArgs, loading?: Boolean) {
        return this.http.get('/sale/order', params, options, loading);
    }

    getOrderById(id: Number) {
        return this.http.get('/sale/order/'+id);
    }

    getOrderStatus() {
        return this.http.get('/sale/order/status');
    }

    getOrderType() {
        return this.http.get('/sale/order/type');
    }

    getOrderSource() {
        return this.http.get('/sale/order/source');
    }

    getOrderPaymentMethod() {
        return this.http.get('/sale/order/payment-method');
    }

    updateOrderById(id: Number, params: Object) {
        return this.http.put('/sale/order/' + id, params);
    }

    updateAddressByOrderId(id: Number, params: Object) {
        return this.http.put('/sale/address/' + id, params);
    }

    getProduct(params?: Object, options?: RequestOptionsArgs, loading?: Boolean) {
        return this.http.get('/sale/product', params, options, loading);
    }

    getProductStatus() {
        return this.http.get('/sale/product/status');
    }

    getConfig() {
        return this.http.get('/sale/common/config');
    }

    getUser() {
        return this.http.get('/sale/user');
    }

    assignByOrderId(id: Number, params: Object) {
        return this.http.put('/sale/assign/' + id, params);
    }

    commentByOrderId(params: Object) {
        return this.http.post('/sale/comment', params);
    }

    generatePurchases(params: Object) {
        return this.http.post('/sale/product/purchases', params);
    }
}