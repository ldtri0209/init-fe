import {Injectable} from '@angular/core';
import {HttpClient} from '../_libraries/http.client';
import {RequestOptionsArgs} from '@angular/http';

@Injectable()
export class PurchaseService {
    constructor(
        public http: HttpClient
    ) {}

    get(params?: Object, options?: RequestOptionsArgs, loading?: Boolean) {
        return this.http.get('/purchase', params, options, loading);
    }

    getProduct(params?: Object, options?: RequestOptionsArgs, loading?: Boolean) {
        return this.http.get('/purchase/product', params, options, loading);
    }

    getConfig() {
        return this.http.get('/purchase/common/config');
    }

    create(params: Object) {
        return this.http.post('/purchase', params);
    }

    getById(id: Number) {
        return this.http.get('/purchase/'+id);
    }

    updateById(id: Number, params: Object) {
        return this.http.put('/purchase/'+id, params);
    }

    updateByDetailId(id: Number, params: Object) {
        return this.http.put('/purchase/detail/'+id, params);
    }

    removeById(id: Number) {
        return this.http.delete('/purchase/'+id);
    }
}