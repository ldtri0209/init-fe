import {Injectable} from '@angular/core';
import {HttpClient} from '../_libraries/http.client';
import {RequestOptionsArgs} from '@angular/http';

@Injectable()
export class StockService {
    constructor(
        public http: HttpClient
    ) {}

    get(params?: Object, options?: RequestOptionsArgs, loading?: Boolean) {
        return this.http.get('/stock', params, options, loading);
    }
}