const webpack = require('webpack');
const helpers = require('./helpers');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: {
        'polyfills': helpers.root('src/polyfills.ts'),
        'vendor': helpers.root('src/vendor.ts'),
        'app': helpers.root('src/main.ts')
    },

    resolve: {
        extensions: ['.ts', '.js'],
        modules: ['node_modules', 'bower_components'],
        alias: {
            'jquery': helpers.root('node_modules/jquery/src/jquery'),
            'FileSaver': helpers.root('node_modules/file-saver/FileSaver'),
            'Highcharts': helpers.root('node_modules/highcharts/highcharts'),
            'swal': helpers.root('node_modules/sweetalert2')
        }
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: [/\.(spec|e2e)\.ts$/],
                loaders: ['awesome-typescript-loader', 'angular2-router-loader', 'angular2-template-loader']
            },
            {
                test: /\.html$/,
                use: 'html-loader'
            },
            {
                test: /\.(png|jpe?g|gif|ico)([\?]?.*)$/,
                use: 'file-loader?name=assets/images/[name].[ext]'
            },
            {
                test: /\.(svg|woff|woff2|ttf|eot)([\?]?.*)$/,
                use: 'file-loader?name=assets/fonts/[name].[ext]'
            },
            {
                test: /\.css$/,
                exclude: helpers.root('src','app'),
                use: ExtractTextPlugin.extract({fallback: 'style-loader', use: 'css-loader?sourceMap'})
            },
            {
                test: /\.css$/,
                include: helpers.root('src','app'),
                use: 'raw-loader'
            },
            {
                test: /\.less$/,
                exclude: helpers.root('src','app'),
                use: ExtractTextPlugin.extract({fallback: 'style-loader', use: ['css-loader','less-loader']})
            },
            {
                test: /\.less$/,
                include: helpers.root('src','app'),
                use: ['raw-loader','less-loader']
            },
            {
                test: /\.(sass|scss)$/,
                exclude: helpers.root('src','app'),
                use: ExtractTextPlugin.extract({fallback: 'style-loader', use: ['css-loader','sass-loader?includePaths[]='+helpers.root('node_modules/compass-mixins/lib')]})
            },
            {
                test: /\.(sass|scss)$/,
                include: helpers.root('src','app'),
                use: ['raw-loader','sass-loader?includePaths[]='+helpers.root('node_modules/compass-mixins/lib')]
            }
        ]
    },

    plugins: [
        new webpack.ContextReplacementPlugin(
            // The (\\|\/) piece accounts for path separators in *nix and Windows
            // /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
            /angular(\\|\/)core(\\|\/)@angular/,
            helpers.root('src'), // location of your src
            {} // a map of your routes
        ),

        new webpack.optimize.CommonsChunkPlugin({
            name: ['app', 'vendor', 'polyfills']
        }),

        new CopyWebpackPlugin([
            {from: helpers.root('src/assets/images'), to: 'assets/images'},
            {from: helpers.root('src/assets/js'), to: 'assets/js'}
        ]),

        new HtmlWebpackPlugin({
            template: helpers.root('src/index.html')
        }),

        new webpack.ProvidePlugin({
            jQuery: 'jquery',
            $: 'jquery',
            'window.jQuery': 'jquery',
            moment: 'moment',
            FileSaver: 'FileSaver',
            Highcharts: 'Highcharts',
            swal: 'swal'
        })
    ]
};